import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) throws SQLException {
		Connection conexao = Conexao.getConnection();
		System.out.println("Sucesso");
		conexao.close();

		Aluno aluno = new Aluno();
		AlunoDao alunoBD = new AlunoDao();
//
//		aluno.setFirst_name("Jose");
//		aluno.setLast_name("da Silva");
//		aluno.setSerie("1");
//		aluno.setTurma("A");
//		aluno.setRua("taquari");
//		aluno.setNumero("1234");
//		aluno.setCep("03166001");
//
//
//		alunoBD.inserir(aluno);
		
		
		//List<Aluno> lista = alunoBD.consultar();
		
		List<Aluno> lista = alunoBD.consultarFiltro("941");
		
		for(Aluno aluno2: lista) {
			System.out.println(aluno2.getFirst_name() + " " + aluno2.getLast_name() + " " + aluno2.getNumero());
		}

		//alunoBD.apaga(1);
//		alunoBD.update(2, "1");
//
//		lista = alunoBD.consultar();
//		
//		for(Aluno aluno2: lista) {
//			System.out.println(aluno2.getFirst_name() + " " + aluno2.getLast_name());
//		}		
		

	}



}
