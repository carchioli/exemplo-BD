import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

public class AlunoDao {

	private Connection conexao;

	public AlunoDao() {
		this.conexao = Conexao.getConnection();
	}

	public void inserir(Aluno aluno) {

		String query = "insert into aluno (first_name, last_name, serie, turma, rua, numero, cep)" +
				"values (?, ?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, aluno.getFirst_name());
			statement.setString(2, aluno.getLast_name());
			statement.setString(3, aluno.getSerie());
			statement.setString(4, aluno.getTurma());
			statement.setString(5, aluno.getRua());
			statement.setString(6, aluno.getNumero());
			statement.setString(7, aluno.getCep());

			statement.execute();

			statement.close();

		}catch(Exception e) {
			e.printStackTrace();
		}


	}


	public List<Aluno> consultar(){

		String query = "select * from aluno";
		List<Aluno> alunos = new ArrayList<Aluno>();

		try {
			PreparedStatement statement = conexao.prepareStatement(query);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				Aluno aluno = new Aluno();
				aluno.setFirst_name(resultado.getString("first_name"));
				aluno.setLast_name(resultado.getString("last_name"));
				aluno.setSerie(resultado.getString("serie"));
				aluno.setTurma(resultado.getString("turma"));
				aluno.setRua(resultado.getString("rua"));
				aluno.setNumero(resultado.getString("numero"));
				aluno.setCep(resultado.getString("cep"));

				alunos.add(aluno);
			}

			statement.close();
			resultado.close();

		}catch(Exception e) {
			e.printStackTrace();
		}

		return alunos;

	}

	public void apaga(int id) {
		String query = "delete from aluno where id_matricula = ?";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			
			statement.setInt(1, id);
			
			statement.execute();
			
			statement.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void update(int parametro, String valor) {

		String query = "update aluno set numero = ? where id_matricula = ?";
				
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, valor);
			statement.setInt(2, parametro);

			statement.execute();

			statement.close();

		}catch(Exception e) {
			e.printStackTrace();
		}


	}	

	public List<Aluno> consultarFiltro(String filtro){

		String query = "select * from aluno where numero = ?";
		
		List<Aluno> alunos = new ArrayList<Aluno>();

		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			
			statement.setString(1, filtro);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				Aluno aluno = new Aluno();
				aluno.setFirst_name(resultado.getString("first_name"));
				aluno.setLast_name(resultado.getString("last_name"));
				aluno.setSerie(resultado.getString("serie"));
				aluno.setTurma(resultado.getString("turma"));
				aluno.setRua(resultado.getString("rua"));
				aluno.setNumero(resultado.getString("numero"));
				aluno.setCep(resultado.getString("cep"));

				alunos.add(aluno);
			}

			statement.close();
			resultado.close();

		}catch(Exception e) {
			e.printStackTrace();
		}

		return alunos;

	}	
	
}
